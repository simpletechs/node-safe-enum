const SafeEnum = require('./index');

const e = SafeEnum(['A', 'B', 'C']);
console.log(e.A);
console.log(e.B);
console.log(e.A | e.B | e.C);

// SafeEnums are immutable
e.D = 'this will break';

// thus, this will throw an error
console.log(e.D);